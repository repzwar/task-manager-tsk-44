package ru.pisarev.tm.api;


import ru.pisarev.tm.dto.AbstractDtoEntity;

public interface IServiceDto<E extends AbstractDtoEntity> extends IRepositoryDto<E> {

}