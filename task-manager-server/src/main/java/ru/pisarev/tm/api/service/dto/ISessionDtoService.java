package ru.pisarev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IServiceDto;
import ru.pisarev.tm.dto.SessionDto;
import ru.pisarev.tm.dto.UserDto;
import ru.pisarev.tm.enumerated.Role;

import java.util.List;

public interface ISessionDtoService extends IServiceDto<SessionDto> {

    SessionDto open(@Nullable String login, @Nullable String password);

    UserDto checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull SessionDto session, Role role);

    void validate(@Nullable SessionDto session);

    SessionDto sign(@Nullable SessionDto session);

    void close(@Nullable SessionDto session);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<SessionDto> findAllByUserId(@Nullable String userId);
}
