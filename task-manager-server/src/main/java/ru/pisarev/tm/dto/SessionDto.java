package ru.pisarev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tm_session")
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionDto extends AbstractBusinessDtoEntity implements Cloneable {

    @Override
    public SessionDto clone() {
        try {
            return (SessionDto) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Column
    @Nullable
    private Long timestamp;

    @Column
    @Nullable
    private String signature;

    @Override
    public String toString() {
        return timestamp + id + userId;
    }
}
