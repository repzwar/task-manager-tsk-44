package ru.pisarev.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.service.IConnectionService;
import ru.pisarev.tm.api.service.ILogService;
import ru.pisarev.tm.api.service.ServiceLocator;
import ru.pisarev.tm.api.service.dto.*;
import ru.pisarev.tm.api.service.model.IProjectService;
import ru.pisarev.tm.api.service.model.ISessionService;
import ru.pisarev.tm.api.service.model.ITaskService;
import ru.pisarev.tm.api.service.model.IUserService;
import ru.pisarev.tm.component.Backup;
import ru.pisarev.tm.dto.ProjectDto;
import ru.pisarev.tm.dto.TaskDto;
import ru.pisarev.tm.dto.UserDto;
import ru.pisarev.tm.endpoint.*;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.service.ConnectionService;
import ru.pisarev.tm.service.DataService;
import ru.pisarev.tm.service.LogService;
import ru.pisarev.tm.service.PropertyService;
import ru.pisarev.tm.service.dto.*;
import ru.pisarev.tm.service.model.ProjectService;
import ru.pisarev.tm.service.model.SessionService;
import ru.pisarev.tm.service.model.TaskService;
import ru.pisarev.tm.service.model.UserService;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ru.pisarev.tm.util.SystemUtil.getPID;
import static ru.pisarev.tm.util.TerminalUtil.displayWelcome;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskDtoService taskDtoService = new TaskDtoService(connectionService);

    @NotNull
    private final IProjectDtoService projectDtoService = new ProjectDtoService(connectionService);

    @NotNull
    private final IProjectTaskDtoService projectTaskDtoService = new ProjectTaskDtoService(connectionService);

    @NotNull
    private final IUserDtoService userDtoService = new UserDtoService(connectionService, propertyService);

    @NotNull
    private final ISessionDtoService sessionDtoService = new SessionDtoService(connectionService, userDtoService, propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, userService, propertyService);

    @NotNull
    private final DataService dataService = new DataService(userDtoService, taskDtoService, projectDtoService, sessionDtoService);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this, projectDtoService, projectTaskDtoService, projectService);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this, sessionDtoService, userDtoService, sessionService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this, taskDtoService, projectTaskDtoService, taskService);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this, userDtoService, userService, sessionDtoService);

    @NotNull
    private final DataEndpoint dataEndpoint = new DataEndpoint(this, dataService);

    public void start(String... args) {
        displayWelcome();
        process();
        //backup.init();
        initEndpoint();
        initData();
    }

    public void initApplication() {
        initPID();
    }

    private void initData() {
        final String admin = userDtoService.add("admin", "admin", "admin@a").getId();
        @NotNull final UserDto user = userDtoService.add("user", "user");

        projectDtoService.add(admin, new ProjectDto("Project C", "-")).setStatus(Status.COMPLETED);
        projectDtoService.add(admin, new ProjectDto("Project A", "-"));
        projectDtoService.add(admin, new ProjectDto("Project B", "-")).setStatus(Status.IN_PROGRESS);
        projectDtoService.add(admin, new ProjectDto("Project D", "-")).setStatus(Status.COMPLETED);
        taskDtoService.add(admin, new TaskDto("Task C", "-")).setStatus(Status.COMPLETED);
        taskDtoService.add(admin, new TaskDto("Task A", "-"));
        taskDtoService.add(admin, new TaskDto("Task B", "-")).setStatus(Status.IN_PROGRESS);
        taskDtoService.add(admin, new TaskDto("Task D", "-")).setStatus(Status.COMPLETED);
    }


    public void initEndpoint() {
        initEndpoint(projectEndpoint);
        initEndpoint(sessionEndpoint);
        initEndpoint(taskEndpoint);
        initEndpoint(adminEndpoint);
        initEndpoint(dataEndpoint);
    }

    public void initEndpoint(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void process() {
        logService.debug("Test environment.");
        @Nullable String command = "";

    }

}
