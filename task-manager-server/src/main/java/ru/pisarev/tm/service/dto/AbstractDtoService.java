package ru.pisarev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.IServiceDto;
import ru.pisarev.tm.api.service.IConnectionService;
import ru.pisarev.tm.dto.AbstractDtoEntity;

public abstract class AbstractDtoService<E extends AbstractDtoEntity> implements IServiceDto<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
