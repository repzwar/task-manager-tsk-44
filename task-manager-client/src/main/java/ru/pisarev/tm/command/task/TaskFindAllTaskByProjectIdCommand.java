package ru.pisarev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.TaskAbstractCommand;
import ru.pisarev.tm.endpoint.TaskDto;
import ru.pisarev.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllTaskByProjectIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-find-by-project-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all task in project by project id.";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final List<TaskDto> tasks = serviceLocator.getTaskEndpoint().findTaskByProjectId(getSession(), id);
        System.out.println("TaskDto list for project");
        for (@NotNull TaskDto task : tasks) {
            System.out.println(task.toString());
        }
    }
}
