package ru.pisarev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        for (@NotNull final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command.toString());
        }
    }
}
