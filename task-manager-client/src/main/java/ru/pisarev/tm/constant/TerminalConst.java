package ru.pisarev.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String CMD_EXIT = "exit";

}
